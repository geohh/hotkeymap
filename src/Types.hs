{-# LANGUAGE DeriveGeneric #-}

module HotkeyMap.Types (Scope, Hotkey) where

import Foundation
import GHC.Generics
import Data.Aeson
import Data.Text

newtype Scope = Scope [Hotkey]
                deriving (Show, Generic)

instance ToJSON   Scope
instance FromJSON Scope

data Hotkey = Hotkey {
            key :: Char,
            mods :: !Text,
            action :: !Text,
            description :: !Text
            } deriving (Show, Generic)

instance ToJSON   Hotkey
instance FromJSON Hotkey

